# BNN-ternary-errors
This code trains an MLP on the MNIST database using binary-valued neurons and ternary back-propagated errors. The full training scheme is described in [Hardware-Efficient On-line Learning through Pipelined Truncated-Error Backpropagation in Binary-State Networks](https://www.frontiersin.org/articles/10.3389/fnins.2017.00496/full). You have to be careful while setting the learning rate as the effective learning rate will depend on the number of bits as well as the error configuration (ternary or not). The learning rate is dropped by half every 10 epochs. When using ternary errors, the learning rate will not drop below 1. See the examples for some sensible choices of learning rate.

## Setup
Install the required python packages using:
```bash
pip install -r requirements.txt 
```

## Usage
```
usage: main.py [-h] [--epochs EPOCHS] [--n-hidden N_HIDDEN] [--n-neurons N_NEURONS] [--n-bits N_BITS] [-b BATCH_SIZE] [--lr LR] [--vanilla-network] [--ternary-errors] [--bipolar-activations]

Binary networks with ternary errors

optional arguments:
  -h, --help            show this help message and exit
  --epochs EPOCHS       number of total epochs to run (default:100)
  --n-hidden N_HIDDEN   number of hidden layers (default:2)
  --n-neurons N_NEURONS
                        number of neurons in hidden layer (default:600)
  --n-bits N_BITS       number of bits per weight (default:8)
  -b BATCH_SIZE, --batch-size BATCH_SIZE
                        mini-batch size (default: 100)
  --lr LR, --learning-rate LR
                        initial learning rate (default:0.1)
  --vanilla-network     Use a vanilla network with ReLUs and full-precision errors and weights. (default: False)
  --ternary-errors      Use ternary errors (default: False). Only effective if --vanilla-network is not specified.
  --bipolar-activations
                        Use bipolar +1/-1 activations, otherwise, use unipolar +1/0 activations. Only effective if --vanilla-network is not specified. (default: False)
```

## Examples
Train a binary network with ternary errors and 8-bit weights
```bash
python main.py --ternary-errors --n-bits 8 --lr 1
```
Train a binary network with ternary errors and 16-bit weights
```bash
python main.py --ternary-errors --n-bits 16 --lr 8
```

Train a binary network with true errors and 8-bit weights
```bash
python main.py --n-bits 8 --lr 0.01
```

Train a standard MLP
```bash
python main.py --vanilla-network --lr 0.01
```