import torch
import torch.nn as nn
from integer_base import *

class mnist_mlp(nn.Module):

    def __init__(self, vanilla_mlp, n_hidden_layers, neurons_per_layer, nbits, bipolar, ternary_error,input_dropout_prob,inter_layer_dropout_prob):
        super(mnist_mlp, self).__init__()


        layer_list = []
        nonLinearity = nn.ReLU() if vanilla_mlp else BinaryActivationFactory(bipolar,ternary_error,2**nbits,1.0e-3)
        
        for i in range(n_hidden_layers + 1):
            input_dim = 784 if i==0 else neurons_per_layer
            output_dim = 10 if i==n_hidden_layers else neurons_per_layer
            max_float = 0.5#2.0 if i==n_hidden_layers else 0.5
            layer_list.append(nn.Dropout(p = input_dropout_prob if i==0 else inter_layer_dropout_prob))
            if vanilla_mlp:
                layer_list.append(nn.Linear(input_dim,output_dim,bias=False))
            else:
                layer_list.append(IntLinear(input_dim,output_dim,nbits,max_float))

            if i != n_hidden_layers:    
                layer_list.append(nonLinearity)
                
        self.layer_stack = nn.Sequential(*layer_list)

    def integerize(self):
        [x.integerize() for x in self.modules() if isinstance(x,IntTensor)]

    def forward(self, x):
        return self.layer_stack(x.view(-1,784))
